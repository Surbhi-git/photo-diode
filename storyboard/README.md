## Round 2

Experiment 1: Operating principle of photo diode 

### 1. Story Outline:

<div align="justify">Photo means light and diode means a device consisting of two electrodes.A photo diode is a light sensitive electronic device capable of converting light into a voltage or current signal. It works on the principle of photo generation.
Some photodiodes will look like a light emitting diode. They have two terminals coming from the end. The smaller end of the diode is the cathode terminal, while the longer end of the diode is the anode terminal.
The working principle of a photodiode is, when a photon of ample energy strikes the diode, it makes a couple of an electron-hole. This mechanism is also called as the inner photoelectric effect. If the absorption arises in the depletion region junction, then the carriers are removed from the junction by the inbuilt electric field of the depletion region. Therefore, holes in the region move toward the anode, and electrons move toward the cathode, and a photocurrent will be generated. The entire current through the diode is the sum of the absence of light and the photocurrent. So the absent current must be reduced to maximize the sensitivity of the device.


### 2. Story:

#### 2.1 Set the Visual Stage Description:
<h2>Construction of the set-up</h2>
For better visualization, a simulator is provided.In this there are three sliders for voltage, intensity and resistance respectively.Resistance is kept constant.There is video which is showing us the variation in current on changing the intensity of light falling on the diode.There is a circuit in which there are two guages ,one for ammeter which measures current other for voltmeter showing voltage values.Sliders are connected with with guages.


#### 2.2 Set User Objectives & Goals:
Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>recall the concept of photo-diode. <br> | Recall | Identify
2.| User will be able to: <br>understand the change in current<br> with change in voltage and intensity values <br>in a photo-diode.  | Understand| Describe
3.| User will be able to: <br>apply different intensities of light to see the current variation. | Apply | Implement
4.| User will be able to: <br>To analyze the variation of current with intensity in photo-diode.<br> | Analyze| Examine

Enhance conceptual and logical skill
</b>

#### 2.3 Set the Pathway Activities:

The simulator tab would allow:<br> <br>
<dd>1.	The set-up consists of a circuit, two guages ,one for ammeter and other for voltmeter.<br>
2.	Input fields to get Velocity and Intensity of Photo-diode.<br>
3.	There would be ‘Sliders’ through which we can change the voltage and intensity values.<br>

</dd>


##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:

Assessment Questions:<br>
Task 1: Recalling The Photo-diode<br>

<dd><b>1 : Photo-diode is which type of diode:-<br>
a)	P-N junction diode.<br>
b)	Zener diode.<br>
c)	Schottky diode.<br>
d)	None Of these<br></dd><br></b>
</dd>
Task 2: Use of Photo-diode:-<br>

<dd><b>2:Photo-diode is used for the detection of:-<br>
a)   Visible light.<br>
b)   Invisible light.<br>
c)   Both a) and b).<br>
d)   none.<br></b>
</dd>

Task 3: To understand the reason behind reverse-biased of diode and make student friendly with diode:-<br>

<dd><b>1.In using a photo diode as a photo detector, it is invariably reverse biased. Why?.<br>
a)The power consumption is much reduced compared to reverse biased condition<br>
b) Electron hole pairs can be produced by the incident photons only if the photo diode is reverse biased<br>
c) When photons are incident on the diode, the fractional change in the reverse current is much greater than the fractional change in the forward current.<br>
d)	None of these<br></dd><br></b>



##### 2.6 Conclusion:
<dd>In this we are able to know that how current gets changed when we change the intensity of light .
On changing the intensity and voltage values the current value will change and it will shown by the meter.
</dd>

##### 2.7 Equations/formulas: NA


### 3. Flowchart
<img src="C:\Users\Surbhi Maheshwari\Desktop\bootathon\storyboard\flowchart\Flow chart.jpeg" style="height:50rem;" alt="Flow Chart Image here"/>

### 4. Mindmap
<img src="C:\Users\Surbhi Maheshwari\Desktop\bootathon\storyboard\mindmap\mindmap.png" alt="mindmap Image here"/>
 
### 5. Storyboard 
<img src="C:\Users\Surbhi Maheshwari\Desktop\bootathon\storyboard\storyboard\storyboard.gif" alt="Gif here">
