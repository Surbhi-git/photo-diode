## Round 0

<b>Discipline | <b>Electrical and Electronics
:--|:--|
<b>Lab</b> | <b>Analog Electronics Lab</b>
<b>Experiment</b>| <b> Operating Principle of photo-diode</b>

<h5> About the Experiment : </h5>
In this experiment we are showing the variation of voltage and current on changing the intensity of light falling on photo-diode. 

<b>Name of Developer |Sawan Kumar Sharma<b> 
:--|:--|
Institute | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh
Email id| sawan.sks69@gmail.com
Department | Electrical and Electronics Engineering(Assistant Professor)


#### Contributors List

SrNo | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Sawan Kumar Sharma| Faculty | Electrical and Electronics Engineering(Assistant Professor) | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | sawan.sks69@gmail.com
2 | Nikhil Kumar | Student | Dept. of Electronics and Communication | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | nikhilk9956@gmail.com
3 | Surbhi Maheshwari | Student | Dept. of Computer Science and Engineering | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | surbhi.godani10@gmail.com
4 | Swali Saxena | Student | Dept. of Computer Science and Engineering | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | swalisaxena6@gmail.com
5 | Vaishali Pandey | Student | Dept. of Computer Science and Engineering | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | vaishali16pandey@gmail.com


