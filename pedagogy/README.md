## Round 1
<p align="center">

<b> Operating Principle of photo-diode </b> <a name="top"></a> <br>
</p>

<b>Discipline | </b> Basic Electronics
:--|:--|
<b> Lab</b> | Analog Electronics Lab
<b> Experiment</b>| Operating Principle of photo-diode


<h4> [1. Focus Area](#LO)
<h4> [2. Learning Objectives ](#LO)
<h4> [3. Instructional Strategy](#IS)
<h4> [4. Task & Assessment Questions](#AQ)
<h4> [5. Simulator Interactions](#SI)
<hr>

<a name="LO"></a>
#### 1. Focus Area : Reinforce theoretical concept and Experimentation
#### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>recall the concept of photo-diode. | Recall | Recall
2.| User will be able to: <br>understand the change in current<br>with change in voltage and intensity values<br> in a photo-diode.  | Understand| Describe
3.| User will be able to: <br>experiment with the photo-diode for varying light intensities. | Experiment | Experimentation
4.| User will be able to: <br>To analyze the variation of current with intensity in photo-diode. | Analyze| Examine




<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy  :     <u> Expository and problem based.</u>
###### Assessment Method: <u>Formative Assessment and Summative Assessment</u>

<u> <b>Description: </b> In this experiment you will learn the variation of current with intensity: </u>
<br>
 <div align="justify">•	The main objective to develop this lab is to provide an interactive source of learning for the students. The simulation that we provide fulfills our purpose.
<br>
•	The learner will be easily able to understand Basic Electronics.<br>
•	The user will able to understand a relation between current and intensity.<br>
•	With the help of our virtual lab, students get a chance to learn photo-diode as they are provided with an interactive simulator. It is beneficial in understanding the basics of Electronics which simply cannot be understood by self-evaluation.
<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:

Read the theory and comprehend the concepts related to the experiment.
<br>
Task 1: Recalling The Photo-diode<br>

<dd><b>1 : Photo-diode is which type of diode:-<br>
a)	P-N junction diode.<br>
b)	Zener diode.<br>
c)	Schottky diode.<br>
d)	None Of these<br></dd><br></b>
</dd>
Task 2: Understanding the principle of Photo-diode:-<br>

<dd><b>2: What will be the change in current when we increase the intensity of light falling on the diode:-<br>
a)   Increase.<br>
b)   Decrease.<br>
c)   Remain same.<br>
d)   First increse then decrease.<br></b>
</dd>

Task 3: To understand the reason behind reverse-biased of diode and make student friendly with diode:-<br>

<dd><b>1.In using a photo diode as a photo detector, it is invariably reverse biased. Why?.<br>
a)The power consumption is much reduced compared to reverse biased condition<br>
b) Electron hole pairs can be produced by the incident photons only if the photo diode is reverse biased<br>
c) When photons are incident on the diode, the fractional change in the reverse current is much greater than the fractional change in the forward current.<br>
d)	None of these<br></dd><br></b>

 <!-- <img src="rocket.png" alt="Put Image Of Our SpaceShip In Space">
 <br> -->

 <br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

What Students will do? |	What Simulator will do?	| Purpose of the task
:--|:--|:-:
Take note of the instructions. | Display the simulator contents.  |<div align = "justify">  Display simulator interface.
Click on Start Experiment Button  | Start The Experiment  |<div align = "justify"> To start the experiment.
|Click on start button | Basic question on photo-diode will be poped. |<div align="justify"> To interact with the user.
  | On sliding the slider of voltage |Simulator will display the variation in voltmeter and ammeter.|<div align = "justify"> To show the variation of current with respect to voltage.
 | On sliding the slider of intensity| Simulator will display the variation in ammeter. |<div align = "justify">To show the variation of current with respect to intensity.
