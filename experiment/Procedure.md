### Procedure
 ☞Connect +12V adapter LED module and Photo Diode module.

☞Switch (sw1) ON the LED Module and Multi meter.

☞Connect P1 and P6 test point, P2 and P7 test point using patch chord in LED module.

☞Vary the DC Source at maximum position.

☞Connect Fiber cable between LED and Photo Diode module.

☞Now measure the series Resistance R in Photo Diode module.

☞Switch (sw1) ON the Photo Diode module

☞Connect the multi meter probe, positive to P5 and negative to P6 Ground.

☞Now we get a DC voltage output on multi meter and vary the pot meter min to max range (0V to 5V).

☞Connect P5 and P4 test point, P6 and P3 test point (reversely) using patch chord.
         <h4>Steps of simulator :</h4><br>
          1.&nbsp;Sliding the voltage slider. <br>
          2.&nbsp;Sliding the intensity slider. <br>
          3.&nbsp;Sliding the resistance slider will not result in any type of variation as the resistance is kept constant. <br>