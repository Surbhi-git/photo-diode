

(function() {
  function buildQuiz() {
    const output = [];
    myQuestions.forEach((currentQuestion, questionNumber) => {
      const answers = [];

      for (letter in currentQuestion.answers) {
        answers.push(
          `<label>
            <input type="radio" name="question${questionNumber}" value="${letter}">
            ${letter} :
            ${currentQuestion.answers[letter]}
          </label>`
        );
      }

      output.push(
        `<div class="question"> ${currentQuestion.question} </div>
        <div class="answers"> ${answers.join("")} </div>`
      );
    });

    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    const answerContainers = quizContainer.querySelectorAll(".answers");

    let numCorrect = 0;

    myQuestions.forEach((currentQuestion, questionNumber) => {
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      if (userAnswer === currentQuestion.correctAnswer) {
        numCorrect++;

      } else {
        // if answer is wrong or blank
        // color the answers red
        answerContainers[questionNumber].style.color = "red";
      }
    });

    // show number of correct answers out of total
    resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");
 

  const myQuestions = [
    {
      question: "In photodiode, when there is no incident light, the reverse current is almost negligible and is called-:",  ///// Write the question inside double quotes
      answers: {
        a: "Zener Current",                 
        b: "Dark Current",                  
        c: "Photo Current",                 
        d: "PIN Current"                   
      },
      correctAnswer: "b"
    },

    {
     question: "Photo-diode is used in-:",  
      answers: {
        a: "Smoke detector",                
        b: "Camera light meters",                  
        c: "Photoconductors",                  
        d: "All of these"                  
      },
      correctAnswer: "d"                
    },     
    {
        question: "ON increasing the intensity of light incidenting on the photo-diode the current will_______ -:",  ///// Write the question inside double quotes
         answers: {
           a: "Increase",                  
           b: "Decrease",                  
           c: "Remain Same",                  
           d: "First increase then decrease"                   
         },
         correctAnswer: "a"                
    },   
       {
        question: "Photo-diode operate by absorption of photons or charged particles and generate a flow of current in an external circuit,___________ to the incident power. The light is absorbed___________ with distance and is___________ to the absorption coefficient-:"
        answers: {
           a: "Proportional, exponentially, proportional.",                  
           b: "Proportional, Logarithmically, inversely proportional.",                 
           c: "Inversely proportional, exponentially, unrelated.",                  
           d: "Inversely proportional, lograithmically, unrelated."                 
        },
         correctAnswer: "a"               
       }                          
  ];




/////////////////////////////////////////////////////////////////////////////

/////////////////////// Do not modify the below code ////////////////////////

/////////////////////////////////////////////////////////////////////////////


  // display quiz right away
  buildQuiz();

  // on submit, show results
  submitButton.addEventListener("click", showResults);
})();


/////////////////////////////////////////////////////////////////////////////

/////////////////////// Do not modify the above code ////////////////////////

/////////////////////////////////////////////////////////////////////////////