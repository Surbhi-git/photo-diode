### Theory
 <h3>Principle of Photo-Diode:</h3>
                    
Photodiodes are frequently used photo detectors. They are semiconductor devices which contain a p–n junction, and often an intrinsic
 (undoped) layer between n and p layers. Devices with an intrinsic layer are called P-I-N or PIN photodiodes. Light absorbed in the
  depletion region or the intrinsic region generates electron–hole pairs, most of which contribute to a photocurrent. The photocurrent
   can be quite precisely proportional to the absorbed (or incident) light intensity over a wide range of optical powers.
                    <br><br>
                    <h3>Operation Modes:</h3>
                    Photodiodes can be operated in two very different modes:
                    <h4> 1) Photovoltaic mode:</h4>
                    In this process like a solar cell, the illuminated photodiode generates a voltage which can be measured. However, 
                    the dependence of this voltage on the light power is nonlinear, and the dynamic range is fairly small. Also, the
                     maximum speed is not achieved.
                    <h4> 2) Photoconductive mode:</h4>
                    Here, a reverse voltage is applied to the diode (i.e., a voltage in the direction where the diode is not conducting 
                    without incident light) and measures the resulting photocurrent. (It may also suffice to keep the applied voltage
                     close to zero.) The dependence of the photocurrent on the light power can be very linear over six or more orders 
                     of magnitude of the light power, e.g. in a range from a few nanowatts to tens of milliwatts for a silicon p–i–n 
                     photodiode with an active area of a few mm2. The magnitude of the reverse voltage has nearly no influence on the 
                     photocurrent and only a weak influence on the (typically small) dark current (obtained without light), but a higher
                      voltage tends to make the response faster and also increases the heating of the device.