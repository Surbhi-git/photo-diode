### References
<p style="font-size:100%; margin-top:2%">
                        1.Analog Electronics
                          Book by A.V.Bakshi U.A.Bakshi A.P.Godse
                          2009.
                        <br><br>
                        2.ANALOG ELECTRONICS
                          Textbook by A. KANDASWAMY and ANDRÉ PITTET
                          2009.
                        <br><br>
                        3.Analog Electronics: An Integrated PSpice Approach
                          Book by T. E. Price
                          1997.
                        <br><br>
                        4.BASIC ELECTRONICS: DEVICES, CIRCUITS AND IT FUNDAMENTALS
                         Book by SANTIRAM KAL
                          2009.
                        <br><br>
<h3>Webliography :</h3>
                        <br>
                        1.https://www.pantechsolutions.net/vi-characteristics-of-optical-photo-diode-in-reverse-bias
                        <br>
                        2.https://www.elprocus.com/photodiode-working-principle-applications/
                        <br>
                        <br>
<h3>Additional video links :</h3>
                        <br>
                        1.https://youtu.be/UoHgPZzu_g8
                        <br>
                        2.https://youtu.be/dVtATH_h0Bg
                        <br>
                        3.https://youtu.be/yMmXHg0hRok
                        <br>