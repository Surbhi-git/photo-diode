### Pre Test
1. In photodiode, when there is no incident light, the reverse current is almost negligible and is called-:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">Zener current.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">Dark current.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">Photo current.
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">PIN current.
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2. Photo-diode is used in-:
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">Smoke detector
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">Camera light meters
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">Photoconductors
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">All of these
                        <br><br>
                        <p id = "p2"></p>
                        <br>
                        3. ON increasing the intensity of light incidenting on the photo-diode the current will_______ -:
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">Increase
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">Decrease
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">Remain same
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">First increse and then decrease
                        <br><br>
                        <p id = "p3"></p>
                        <br>
                        4.Photo-diode operate by absorption of photons or charged particles and generate a flow of current in an external circuit,___________ to the incident power. The light is absorbed___________ with distance and is___________ to the absorption coefficient-:
                        <br>
                        A. <input type="radio" name="but4" id="rb41" onclick="click4();">Proportional, exponentially, proportional.
                        <br>
                        B. <input type="radio" name="but4" id="rb42" onclick="click4();">Proportional, Logarithmically, inversely proportional.
                        <br>
                        C. <input type="radio" name="but4" id="rb43" onclick="click4();">Inversely proportional, exponentially, unrelated.
                        <br>
                        D. <input type="radio" name="but4" id="rb44" onclick="click4();">Inversely proportional, lograithmically, unrelated.
                        <br><br>
                        <p id = "p4"></p>
                        <br>