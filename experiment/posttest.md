### Post Test
<p style="font-size:100%; margin-top:2%">
1. Photo-diode is which type of diode-:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">P-N junction diode.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">Zener diode.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">Schottky diode.
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">None of these.
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2. Photo-diode is used for the detection of-:
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">Visible light.
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">Invisible light.
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">No light.
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">Both visible and invisible light.
                        <br><br>
                        <p id = "p2"></p>
                        <br>
                        3. Photo-diode technology was refined in-:
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">1920
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">1940
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">1950
                        <br>
                         D. <input type="radio" name="but2" id="rb24" onclick="click2();">1980