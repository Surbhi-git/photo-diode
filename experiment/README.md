### Aim
To Study the Operating Principle of Photo Diode in Reverse Bias
### Theory
 <h3>Principle of Photo-Diode:</h3>
                    
Photodiodes are frequently used photo detectors. They are semiconductor devices which contain a p–n junction, and often an intrinsic (undoped) layer between n and p layers. Devices with an intrinsic layer are called P-I-N or PIN photodiodes. Light absorbed in the depletion region or the intrinsic region generates electron–hole pairs, most of which contribute to a photocurrent. The photocurrent can be quite precisely proportional to the absorbed (or incident) light intensity over a wide range of optical powers.
                    <br><br>
                    <h3>Operation Modes:</h3>
                    Photodiodes can be operated in two very different modes:
                    <h4>1) Photovoltaic mode:</h4>
                    In this process like a solar cell, the illuminated photodiode generates a voltage which can be measured. However, the dependence of this voltage on the light power is nonlinear, and the dynamic range is fairly small. Also, the maximum speed is not achieved.
                    <h4>2) Photoconductive mode:</h4>
                    Here, a reverse voltage is applied to the diode (i.e., a voltage in the direction where the diode is not conducting without incident light) and measures the resulting photocurrent. (It may also suffice to keep the applied voltage close to zero.) The dependence of the photocurrent on the light power can be very linear over six or more orders of magnitude of the light power, e.g. in a range from a few nanowatts to tens of milliwatts for a silicon p–i–n photodiode with an active area of a few mm2. The magnitude of the reverse voltage has nearly no influence on the photocurrent and only a weak influence on the (typically small) dark current (obtained without light), but a higher voltage tends to make the response faster and also increases the heating of the device.
                    
                

<h4>Material Required :</h4><br>
                       1)Fiber Optics Light Emitting Diode Module - 01

2)Fiber Optics Photo Diode module - 01

3)Plastic Fiber cable 1 meter - 01

4)Multi meter - 01

5)Adapter +12V/ DC - 02

6)Patch Chords - 04
                        <br><br>
### Procedure
 ☞Connect +12V adapter LED module and Photo Diode module.

☞Switch (sw1) ON the LED Module and Multi meter.

☞Connect P1 and P6 test point, P2 and P7 test point using patch chord in LED module.

☞Vary the DC Source at maximum position.

☞Connect Fiber cable between LED and Photo Diode module.

☞Now measure the series Resistance R in Photo Diode module.

☞Switch (sw1) ON the Photo Diode module

☞Connect the multi meter probe, positive to P5 and negative to P6 Ground.

☞Now we get a DC voltage output on multi meter and vary the pot meter min to max range (0V to 5V).

☞Connect P5 and P4 test point, P6 and P3 test point (reversely) using patch chord.
         <h4>Steps of simulator :</h4><br>                 
          1.&nbsp;Sliding the voltage slider. <br>
          2.&nbsp;See the change in the voltage and current values in the circuit.<br>
          3.&nbsp;Sliding the intensity slider. <br>
          4.&nbsp;See the change in current after sliding the intensity slider and you will see that there is no change in voltage on changing the value of intensity.<br>
          5.&nbsp;Sliding the resistance slider will not result in any type of variation as the resistance is kept constant. <br>
### Pre Test
1. In photodiode, when there is no incident light, the reverse current is almost negligible and is called-:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">Zener current.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">Dark current.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">Photo current.
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">PIN current.
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2. Photo-diode is used in-:
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">Smoke detector
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">Camera light meters
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">Photoconductors
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">All of these
                        <br><br>
                        <p id = "p2"></p>
                        <br>
                        3. On increasing the intensity of light incidenting on the photo-diode the current will_______ -:
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">Increase
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">Decrease
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">Remain same
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">First increase and then decrease
                        <br><br>
                        <p id = "p3"></p>
                        <br>
                        4.Photo-diode operate by absorption of photons or charged particles and generate a flow of current in an external circuit,___________ to the incident power. The light is absorbed___________ with distance and is___________ to the absorption coefficient-:
                        <br>
                        A. <input type="radio" name="but4" id="rb41" onclick="click4();">Proportional, exponentially, proportional.
                        <br>
                        B. <input type="radio" name="but4" id="rb42" onclick="click4();">Proportional, Logarithmically, inversely proportional.
                        <br>
                        C. <input type="radio" name="but4" id="rb43" onclick="click4();">Inversely proportional, exponentially, unrelated.
                        <br>
                        D. <input type="radio" name="but4" id="rb44" onclick="click4();">Inversely proportional, lograithmically, unrelated.
                        <br><br>
                        <p id = "p4"></p>
                        <br>
                        

### Post Test
<p style="font-size:100%; margin-top:2%">
                        1. Photo-diode is which type of diode-:
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">P-N junction diode.
                        <br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">Zener diode.
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">Schottky diode.
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">None of these.
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2. Photo-diode is used for the detection of-:
                        <br>
                        A. <input type="radio" name="but2" id="rb21" onclick="click2();">Visible light.
                        <br>
                        B. <input type="radio" name="but2" id="rb22" onclick="click2();">Invisible light.
                        <br>
                        C. <input type="radio" name="but2" id="rb23" onclick="click2();">No light.
                        <br>
                        D. <input type="radio" name="but2" id="rb24" onclick="click2();">Both visible and invisible light.
                        <br><br>
                        <p id = "p2"></p>
                        <br>
                        3. Photo-diode technology was refined in-:
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">1920
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">1940
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">1950
                        <br>
                         D. <input type="radio" name="but2" id="rb24" onclick="click2();">1980
                    <!--Post Test of experiment -->
                    </p>
### References
<p style="font-size:100%; margin-top:2%">
                        1.Analog Electronics
                          Book by A.V.Bakshi U.A.Bakshi A.P.Godse
                          2009.
                        <br><br>
                        2.ANALOG ELECTRONICS
                          Textbook by A. KANDASWAMY and ANDRÉ PITTET
                          2009.
                        <br><br>
                        3.Analog Electronics: An Integrated PSpice Approach
                          Book by T. E. Price
                          1997.
                        <br><br>
                        4.BASIC ELECTRONICS: DEVICES, CIRCUITS AND IT FUNDAMENTALS
                         Book by SANTIRAM KAL
                          2009.
                        <br><br>
                        <h3>Webliography :</h3>
                        <br>
                        1.https://www.pantechsolutions.net/vi-characteristics-of-optical-photo-diode-in-reverse-bias
                        <br>
                        2.https://www.elprocus.com/photodiode-working-principle-applications/
                        <br>
                        <br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.https://youtu.be/UoHgPZzu_g8
                        <br>
                        2.https://youtu.be/dVtATH_h0Bg
                        <br>
                        3.https://youtu.be/yMmXHg0hRok
                        <br>
                  <!--Theory of experiment -->
                    </p>
